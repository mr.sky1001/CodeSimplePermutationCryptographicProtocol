package nk.omsu

import java.io._
import java.util
import java.util.BitSet
/**
  * Created by nk16 on 06.09.16.
  */

object Main {

  def readBytes(file:File):Array[Byte] = {
    val is = new FileInputStream(file)
    val bytesInp = new Array[Byte](file.length.toInt)
    is.read(bytesInp)
    is.close()
    bytesInp
  }
  def printStr(fileName: String, array: Array[Byte]): Unit ={
    val out = new FileOutputStream(new File(fileName))
    out.write(array)
    out.close()
  }
  def encryption(fileNameE:String):Unit = {

    val bytesInp = readBytes(new File(fileNameE))
    var bitsInp = byteArrayToBitArray(bytesInp)
    var bitsOut =new util.BitSet(bytesInp.length * 8)
    var index = 0
    for (i <- 0 until bytesInp.length * 8) {
      for(j <- 0 until 6){
        j match {
          case 0 => if(bitsInp.get(index + 2)) bitsOut.set(index)
          case 1 => if(bitsInp.get(index + 3)) bitsOut.set(index)
          case 2 => if(bitsInp.get(index - 2)) bitsOut.set(index)
          case 3 => if(bitsInp.get(index + 2)) bitsOut.set(index)
          case 4 => if(bitsInp.get(index - 3)) bitsOut.set(index)
          case 5 => if(bitsInp.get(index - 2)) bitsOut.set(index)
        }
        index +=1
      }
    }
    println("encryption:")
   /* for (i <- 0 until  bytesInp.length * 8)
      if (bitsOut.get(i))
        print(1)
      else
        print(0)*/
    println()
    var bytesOut:Array[Byte] = bitsOut.toByteArray
    printStr(fileNameE, bytesOut)
  }
  def byteArrayToBitArray(bytes: Array[Byte]): util.BitSet = {

    var bits:util.BitSet = util.BitSet.valueOf(bytes)
    println("read:")
  /*  for (i <- 0 until  bytes.length * 8)
      if (bits.get(i))
        print(1)
      else
        print(0)
    println()*/
    bits
  }
  def decryption(fileNameD:String):Unit ={

    val bytesInp = readBytes(new File(fileNameD))
    var bitsInp = byteArrayToBitArray(bytesInp)
    var bitsOut = new util.BitSet(bytesInp.length * 8)
    var index = 0
    for (i <- 0 until bytesInp.length * 8) {
      for (j <- 0 until 6) {
        j match {
          case 0 => if (bitsInp.get(index + 2)) bitsOut.set(index)
          case 1 => if (bitsInp.get(index + 3)) bitsOut.set(index)
          case 2 => if (bitsInp.get(index - 2)) bitsOut.set(index)
          case 3 => if (bitsInp.get(index + 2)) bitsOut.set(index)
          case 4 => if (bitsInp.get(index - 3)) bitsOut.set(index)
          case 5 => if (bitsInp.get(index - 2)) bitsOut.set(index)
        }
        index += 1
      }
    }
    println("decryption:")
  /*  for (i <- 0 until  bytesInp.length * 8)
      if (bitsOut.get(i))
        print(1)
      else
        print(0)*/
    var bytesOut:Array[Byte] = bitsOut.toByteArray
    printStr(fileNameD, bytesOut)
  }
  def main(args:Array[String]) :Unit ={

    val filenameE = readLine("Input: ")// "/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/Lab2(scala, sbt, KMZ)/src/main/resources/f.txt"

   //encryption(filenameE)
   decryption(filenameE)
  }
}